#include <iostream>
#include "src/functions/FileFunctions.h"
#include "src/functions/THashDFunctions.h"
#include "src/functions/THashEncFunctions.h"
#include "src/functions/auxForMain.h"

int main(void) {
    const std::string FILE_PATH = "../src/files/LoremIpsum.txt";
    const int FILE_LENGTH = countWords(FILE_PATH);
    const int HASH_LENGTH = FILE_LENGTH + (FILE_LENGTH / 2);

    char choice;
    std::cout << "Digite 'c' para Tabela En[c]adeada ou 'd' para En[d]ereçada: ";
    std::cin >> choice;

    switch (choice) {
        case 'c':
            useTHashEnc(HASH_LENGTH, FILE_LENGTH, FILE_PATH);
            break;
        case 'd':
            useTHashD(HASH_LENGTH, FILE_LENGTH, FILE_PATH);
            break;
        default:
            break;
    }
}