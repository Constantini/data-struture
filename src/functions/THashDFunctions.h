#ifndef DATA_STRUTURE_THASHDFUNCTIONS_H
#define DATA_STRUTURE_THASHDFUNCTIONS_H

#include <string>
#include <iostream>
#include "fstream"
#include "../headers/THashD.h"

template <class type>
void initThashD(THashD<type> table[], int length) {
    for (int i = 0; i < length; i++) {
        table[i].table = "";
    }
}

int tHashDHashing(std::string key, int length) {
    int sum = 0;
    for (int i = 0; i < key.size(); i++) {
        sum += (int)key[i];
    }
    sum = (int)key[0] * sum;
    return sum % length;
}

template <class type>
int getIndexTHashD(THashD<type> table[], std::string key, int length) {
    int i = tHashDHashing(key, length);
    int index = i;
    int controller = 0;
    while (true) {
        if (controller > 0 && i == index) {
            return -1;
        }

        if (table[i].table == key) {
            return i;
        } else if (table[i].table == "") {
            std::cout << i;
            return -1;
        } else if (i + 1 == length) {
            i = -1;
            controller += 1;
        }
        i++;
    }
}

template <class type>
std::string getValueTHashD(THashD<type> table[], std::string key, int length) {
    int i = tHashDHashing(key, length);
    while(true) {
        if (i + 1 == length) {
            i = 0;
        }
        if (table[i].table == key) {
            return  table[i].table;
        } else if (table[i].table == "") {
            return "";
        } else {
            i++;
        }
    }
}

template <class type>
void insertTHashD(THashD<type> table[], std::string key, int length, int &counter) {
    int controller = 0;
    int index = tHashDHashing(key, length);
    for (int i = index; i < length; i++) {
        if (table[i].table == "") {
            table[i].table = key;
            break;
        }

        if (controller > 0 && i == index) {
            std::cout << "Tabela cheia!";
            break;
        }

        if (i + 1 == length) {
            i = -1;
            controller++;
        }

        counter++;
    }
}

template <class type>
void deleteValueTHashD(THashD<type> table[], std::string key, int length) {
    std::string value;
    int i = tHashDHashing(key, length);
    for (i; i < length; i++) {
        if (table[i].table == key) {
            table[i].table = "";
            break;
        }
        if (i + 1 == length) {
            i = -1;
        }
    }
    int p = i + 1;
    while(table[p].table != "") {
        value = table[p].table;
        table[p].table = "";
        insertTHashD(table, value, length);

        if (p + 1 != length) {
            p++;
        } else {
            p = 0;
        }
    }
}

template <class type>
void printTHashD(THashD<type> table[], int length) {
    for (int i = 0; i < length; i++) {
        std::cout << "|" << i+1 << "|" << " -> " << table[i].table << std::endl;
    }
}

#endif //DATA_STRUTURE_THASHDFUNCTIONS_H