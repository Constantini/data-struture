#ifndef DATA_STRUTURE_AUXFORMAIN_H
#define DATA_STRUTURE_AUXFORMAIN_H

#include "iostream"
#include "../headers/THashEnc.h"
#include "TListFunctions.h"
#include "../headers/THashD.h"
#include "../functions/FileFunctions.h"
#include "../functions/THashDFunctions.h"
#include "../functions/THashEncFunctions.h"

void useTHashEnc(const int HASH_LENGTH, const int FILE_LENGTH, std::string FILE_PATH){
    THashEnc<TList<std::string>> tHashEnc[HASH_LENGTH];
    initTHashEnc(tHashEnc, HASH_LENGTH);
    for (int i = 0; i < FILE_LENGTH; i++) {
        insertTHashEnc(tHashEnc, getWords(FILE_PATH, i), HASH_LENGTH);
    }
    printTHashEnc(tHashEnc, HASH_LENGTH);

    std::cout << std::endl << std::endl << "Colisões: " << countCollisionsTHashEnc(tHashEnc, HASH_LENGTH) << std::endl;
}

void useTHashD(const int HASH_LENGTH, const int FILE_LENGTH, std::string FILE_PATH){
    THashD<std::string> tHashD[HASH_LENGTH];
    int couter = 0;
    initThashD(tHashD, HASH_LENGTH);

    for (int i = 0; i < FILE_LENGTH; i++) {
        insertTHashD(tHashD, getWords(FILE_PATH, i), HASH_LENGTH, couter);
    }

    printTHashD(tHashD, HASH_LENGTH);
    std::cout << std::endl << std::endl << "Colisões: " << couter << std::endl;
}
#endif //DATA_STRUTURE_AUXFORMAIN_H
