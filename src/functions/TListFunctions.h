#ifndef DATA_STRUTURE_TLISTFUNCTIONS_H
#define DATA_STRUTURE_TLISTFUNCTIONS_H
#include "iostream"
#include "../headers/TList.h"

template <class type>
void insertList(TList<type>* &tList, std::string data) {
    if (tList != NULL) {
        TElement<type>* newValue = new  TElement<type>;
        newValue->data = data;
        newValue->proximo = tList->start;
        tList->start = newValue;
    } else {
        TElement<type>* newValue = new  TElement<type>;
        newValue->data = data;
        newValue->proximo = NULL;
        TList<type>* newList = new TList<type>;
        newList->start = newValue;
        tList = newList;
    }
}

template <class type>
void deleteList(TList<type>* &tList, std::string key) {
    TElement<type>* nav;
    TElement<type>* del;
    if (tList != NULL && tList->start != NULL) {
        if (tList->start->proximo == NULL) {
            del = tList->start;
            tList->start = NULL;
            delete del;
            return;
        }
        for (nav = tList->start; nav != NULL; nav = nav->proximo) {
            if (nav->proximo == NULL) {
                return;
            } else if (nav->data == key) {
                del = nav;
                nav = nav->proximo;
                tList->start = nav;
                delete del;
                return;
            } else if (nav->proximo->data == key) {
                del = nav->proximo;
                if (nav->proximo->proximo == NULL) {
                    nav->proximo = NULL;
                    delete del;
                    return;
                } else {
                    nav->proximo = nav->proximo->proximo;
                    delete del;
                    return;
                }
            }
        }
    }
}

template <class type>
void printList(TList<type>* &tLista) {
    if (tLista == NULL || tLista->start == NULL) {
        return;
    } else {
        for (TElement<type>* nav = tLista->start; nav != NULL; nav = nav->proximo) {
            std::cout << "|" <<  nav->data << "|";
        }
    }
}

template <class type>
type getValueList(TList<type>* &tList, std::string data) {
    if (tList == NULL || tList->start == NULL) {
        return "";
    } else {
        TElement<type>* nav = tList->start;
        if (nav->data == data) {
            return data;
        }
        for (nav = tList->start; nav != NULL; nav = nav->proximo) {
            if(nav->data == data) {
                return data;
            }
        }
    }
}

#endif //DATA_STRUTURE_TLISTFUNCTIONS_H
