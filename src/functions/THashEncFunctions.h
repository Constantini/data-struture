#ifndef DATA_STRUTURE_THASHENCFUNCTIONS_H
#define DATA_STRUTURE_THASHENCFUNCTIONS_H
#include "iostream"
#include "../headers/THashEnc.h"
#include "TListFunctions.h"
template <class type>
void initTHashEnc(THashEnc<type> table[], int length) {
    for (int i = 0; i < length; i++) {
        table[i].table = NULL;
    }
}

int tHashEncHashing(std::string key, int length) {
    int sum = 0;
    for (int i = 0; i < key.size(); i++) {
        sum += (int)key[i];
    }
    sum = (int)key[0] * sum;
    return sum % length;
}

template <class type>
void insertTHashEnc(THashEnc<type> table[], std::string key, int length) {
    insertList(table[tHashEncHashing(key, length)].table, key);
}

template <class type>
int countCollisionsTHashEnc(THashEnc<TList<type>> table[], int length){
    int collisions = 0;
    for(int i = 0; i < length; i++){
        if(table[i].table != NULL){
            TElement<type>* nav;
            int couter = 0;
            for (nav = table[i].table->start; nav->proximo != NULL; nav = nav->proximo) {
                couter += couter + 1;
            }
            collisions += couter;
        }
    }
    return collisions;
}

template <class type>
void deleteValueTHashEnc(THashEnc<type> table[], std::string key, int length) {
   deleteList(table[tHashEncHashing(key, length)].table, key);
}

template <class type>
int getIndexTHashEnc(THashEnc<type> table[], std::string key, int length) {
    if (table[tHashEncHashing(key, length)].table != NULL) {
        return tHashEncHashing(key, length);
    } else {
        return -1;
    }
}

template <class type>
std::string getValueTHashEnc(THashEnc<type> table[], std::string key, int length) {
    int index = tHashEncHashing(key, length);
    return getValueList(table[index].table, key);
}

template <class type>
void printTHashEnc(THashEnc<type> table[], int length) {
    for (int i = 0; i < length; i++) {
        std::cout << "|" << i << "|" ;
        printList(table[i].table);
        std::cout << std::endl;
    }
}

#endif //DATA_STRUTURE_THASHENCFUNCTIONS_H
