#ifndef DATA_STRUTURE_FILEFUNCTIONS_H
#define DATA_STRUTURE_FILEFUNCTIONS_H

#include <string>
#include <iostream>
#include "fstream"

int countWords(std::string path){
    std::ifstream words(path);
    std::string line = "";
    int lineQtd = 0;

    for (lineQtd = 0; getline(words, line); lineQtd++);

    return lineQtd;
}

std::string getWords(std::string path, int i){
    std::ifstream words(path);
    std::string line = "";
    std::string teste[countWords(path)];

    for (int i = 0; getline(words, line); i++){
        teste[i] = line;
    }

    return teste[i];
}


#endif //DATA_STRUTURE_FILEFUNCTIONS_H