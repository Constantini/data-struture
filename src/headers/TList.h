#ifndef DATA_STRUTURE_TLIST_H
#define DATA_STRUTURE_TLIST_H
#include "iostream"

template <class type>
struct TElement {
    type data;
    TElement<type>* proximo;
};

template <class type>
struct TList {
    TElement<type>* start;
};

#endif //DATA_STRUTURE_TLIST_H
